{-# LANGUAGE OverloadedStrings #-}


module Main where


import Lib
import Network.Wreq
import Data.Aeson (Value)
import Control.Lens


type Resp = Response [String]
type Score = (String, String)


changeWording :: Int -> String
changeWording score =
  case score of
    0 -> "love"
    1 -> "15"
    2 -> "30"
    3 -> "40"
  

calculateScore :: [String] -> IO Score
calculateScore match = do
  let aScore = foldr (\x acc -> if x == "A" then acc + 1 else acc) 0 match
  let bScore = foldr (\x acc -> if x == "B" then acc + 1 else acc) 0 match
  pure $ (changeWording aScore, changeWording bScore)


getMatch :: String -> IO [String]
getMatch url = do
  r <- asJSON =<< get url :: IO Resp
  pure $ r ^. responseBody


main :: IO ()
main = do
  print =<< calculateScore =<< getMatch "https://c5-tennis.herokuapp.com"
